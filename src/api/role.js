import request from '@/utils/request'

const role = {
  getRole: function getRole(data) {
    return request({
      url: '/performance/staff',
      method: 'get',
      params: {
        limit: 10,
        page: 1
      }
    })
  }
}

export default role
