import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/performance/login',
    method: 'post',
    data
  })
}

export function getInfo(name) {
  return request({
    url: '/performance/staff/name',
    method: 'get',
    params: { name: name }
  })
}

export function logout() {
  return request({
    url: '/performance/logout',
    method: 'get'
  })
}
