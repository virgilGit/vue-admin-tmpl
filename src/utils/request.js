import axios from 'axios'
import { Message } from 'element-ui'
import router from '@/router'
import global from '@/utils/constants'
// import store from '@/store'
// import { getToken } from '@/utils/auth'

// create an axios instanc
const service = axios.create({
  baseURL: global.baseUrl, // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    if (response.data.code === 100) {
      router.push(`/login`)
      Message({
        message: response.data.msg,
        type: 'warning'
      })
    } else if (response.data.code !== 0) {
      Message({
        message: response.data.msg,
        type: 'warning'
      })
    }
    return response
  },
  error => {
    if (error.response.status === 401) {
      Message({
        message: '没有当前权限',
        type: 'warning'
      })
    } else {

      Message({
        message: error,
        type: 'error'
      })

      console.log(error)
    }

    return Promise.reject(error, error.response.data)
  }
)

export default service
