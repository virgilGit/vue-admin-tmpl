// import Cookies from 'js-cookie'
// import store from '@/store'

export function getToken() {
  return sessionStorage.getItem('name')
}

export function setToken(token) {
  return sessionStorage.setItem('name', token)
}

export function removeToken() {
  return sessionStorage.removeItem('name')
}
